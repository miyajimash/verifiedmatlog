E computes an interval matrix enclosing log(A) based on Algorithm 1 in [S. Miyajima, Fast enclosure for all eigenvalues and invariant subspaces in generalized eigenvalue problems, SIMAX, 2014]. INTLAB is required for the execution. 

Ms computes an interval matrix enclosing log(A). If successful, it is proved that A has no nonpositive real eigenvalues. If successful, the uniqueness is proved. INTLAB is required for the execution. 

Mj computes an interval matrix enclosing log(A). If successful, it is proved that A has no nonpositive real eigenvalues. If successful, the uniqueness is proved. INTLAB is required for the execution.